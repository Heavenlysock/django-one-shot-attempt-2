"""brain_two URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from todos.views import (
    todo_list_list,
    todo_list_detail,
    todo_list_create,
    todo_list_delete,
    todo_list_update,
    todo_item_create,
    todo_item_update,
)
from django.urls import path

urlpatterns = [
    path("", todo_list_list, name="todos/list.html"),
    path("<int:pk>/", todo_list_detail, name="todo_list_detail"),
    path("create/", todo_list_create, name="todo_list_create"),
    path("<int:pk>/delete/", todo_list_delete, name="todo_list_delete"),
    path("<int:pk>/edit/", todo_list_update, name="todo_list_update"),
    path("items/create/", todo_item_create, name="todo_item_create"),
    path("items/<int:pk>/edit/", todo_item_update, name="todo_item_update"),
]
# urlpatterns = [
#     path("", show_todo_lists, name="todo_list_list"),
#     path("<int:pk>/", show_todo_list, name="todo_list_detail"),
#     path("create/", create_todo_list, name="todo_list_create"),
#     path(
#         "<int:pk>/edit/",
#         update_todo_list,
#         name="todo_list_update",
#     ),

#     ),
#     path(
#         "items/create/",
#         create_todo_item,
#         name="todo_item_create",
#     ),
#     path(
#         "items/<int:pk>/edit/",
#         update_todo_item,
#         name="todo_item_update",
#     ),
# ]
