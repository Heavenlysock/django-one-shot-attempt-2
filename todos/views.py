from django.shortcuts import render, redirect
from .models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm
from django.shortcuts import get_object_or_404

# Create your views here.
def todo_list_list(request):
    todolist = TodoList.objects.all()
    context = {"todo_list_list": todolist}
    return render(request, "list.html", context)


def todo_list_detail(request, pk):
    context = {"todolist": TodoList.objects.get(pk=pk)}
    return render(request, "detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", todolist.pk)
    else:
        form = TodoListForm()
    return render(request, "create.html", {"form": form})


def todo_list_delete(request, pk):
    todolist = get_object_or_404(TodoList, pk=pk)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")
    return render(request, "delete.html")


def todo_list_update(request, pk):
    todolist = get_object_or_404(TodoList, pk=pk)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", todolist.pk)
    else:
        todolist = get_object_or_404(TodoList, pk=pk)
        form = TodoListForm(instance=todolist)
    return render(request, "edit.html", {"form": form})


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", todoitem.list.id)
    else:
        form = TodoItemForm()
    return render(request, "items_create.html", {"form": form})


def todo_item_update(request, pk):
    todoitem = get_object_or_404(TodoItem, pk=pk)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitem)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", todoitem.list.id)
    else:
        form = TodoItemForm(instance=todoitem)
    return render(request, "todo_items/update.html", {"form": form})
